$(document).ready(function() {

  b_find_box('.b_find_box');

});

/* Functions */
function b_find_box (box) {
  $(box).find('.tabs a').click(function() {
    var n = $(this).prevAll().length + 1;
    $(box).find('.tabs').attr('rel', $(this).prevAll().length + 1);
    $(box).find('.el').removeClass('active');
    $(box).find('.el[rel="'+ n +'"]').addClass('active');
    return false;
  });
}